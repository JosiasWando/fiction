from django.db import models

class TipoCategoria(models.Model):
  nome = models.CharField(max_length=50)

  def __str__(self):
      return self.nome

class Categoria(models.Model):
  nome_categoria = models.CharField(max_length=50)
  imagem = models.ImageField(upload_to='categoria_photos' ,max_length=200)
  tipo_categoria = models.ForeignKey(TipoCategoria, on_delete=models.CASCADE)

  def __str__(self):
      return self.nome_categoria
  
      
  
  
    
